import storage from "../Logic";

const handleCreateUser = (socket) => {
  const username = socket.handshake.query.username;
  if (storage.userIsExist(username)) {
    socket.emit("USERNAME_EXISTS", username);
    return false;
  }
  storage.createUser(username, socket.client.id);
  return true;
};

const getActiveRooms = (socket) => {
  const rooms = storage.getActiveRooms();
  socket.emit("ACTIVE_ROOMS", rooms);
};

export { handleCreateUser, getActiveRooms };
