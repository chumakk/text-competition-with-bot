import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../config.js";
import { handleStartGame, readyForGame } from "./gameHandler";
import storage from "../Logic";

const handleJoinRoom = (io, socket, roomName) => {
  const room = storage.getActiveRoom(roomName);

  if (!room) {
    socket.emit("ROOM_NOT_EXIST", roomName);
    return;
  }

  if (room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
    socket.emit("ROOM_IS_FULL", roomName);
    return;
  }

  if (storage.roomIsPlaying(roomName)) {
    socket.emit("ROOM_IS_PLAYING", roomName);
    return;
  }

  const joinedRoom = storage.joinUserToActiveRoom(room, socket.client.id);

  socket.join(roomName);

  if (joinedRoom.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
    io.emit("REMOVE_ROOM", joinedRoom.name);
  }

  socket.broadcast.emit("JOINED_IN_USER", joinedRoom);
  socket.emit("JOINED_IN", joinedRoom);
};

const handleCreateRoom = (io, socket, name) => {
  if (storage.roomIsExist(name)) {
    socket.emit("ROOM_EXISTS", name);
    return;
  }
  const room = storage.createRoom(name, socket.id);

  socket.join(room.name);
  io.emit("ADD_ROOM", room);
  socket.emit("JOINED_IN", room);
};

const maybeStartGameOrAddRoom = (io, room, updatedRoom) => {
  if (storage.roomIsPlaying(updatedRoom.name)) return;

  if (readyForGame(updatedRoom)) {
    handleStartGame(io, updatedRoom);
  } else {
    if (room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
      io.emit("ADD_ROOM", updatedRoom);
    }
  }
}

const leaveRoom = (nameRoom, socket, io) => {
  const room = storage.getActiveRoom(nameRoom);

  const updatedRoom = storage.leaveFromActiveRoom(nameRoom, socket.client.id);

  if (updatedRoom.users.length === 0) {
    storage.deleteActiveRoom(nameRoom);
    storage.deletePlayingRoom(nameRoom);
    io.emit("REMOVE_ROOM", nameRoom);
  } else {
    maybeStartGameOrAddRoom(io, room, updatedRoom);
    io.emit("LEAVED_ROOM", updatedRoom);
  }
}

const handleLeaveRoom = (io, socket) => {
  const rooms = Object.keys(socket.rooms);
  rooms.forEach((nameRoom) => {
    if (storage.roomIsExist(nameRoom)) {
      leaveRoom(nameRoom, socket, io);
      socket.leave(nameRoom);
      socket.emit("SUCCESS_LEAVED");
    }
  });
};

const handleDisconnecting = (io, socket) => {
  storage.deleteUserById(socket.client.id);
  const rooms = Object.keys(socket.rooms);
  rooms.forEach((nameRoom) => {
    if (storage.roomIsExist(nameRoom)) {
      leaveRoom(nameRoom, socket, io);
    }
  });
};

export {
  handleJoinRoom,
  handleCreateRoom,
  handleDisconnecting,
  handleLeaveRoom,
};
