import { texts } from "../../data";
import {
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME,
  MAXIMUM_USERS_FOR_ONE_ROOM,
} from "../config";
import storage from "../Logic";
import commentatorBot from "../CommentatorBot";

const readyForGame = (room) => {
  return room.users.every((user) => {
    return user.isReady === true;
  });
};

const handleToggleStatusReady = (io, socket) => {
  const id = socket.client.id;
  const rooms = Object.keys(socket.rooms);
  rooms.forEach((nameRoom) => {
    if (storage.roomIsExist(nameRoom)) {
      const roomWithUpdatedUserStatus = storage.toggleUserPlayStatus(
        nameRoom,
        id
      );

      io.to(nameRoom).emit(
        "SETETTED_TOGGLE_STATUS_READY",
        roomWithUpdatedUserStatus.users.find((user) => user.id === id)
      );

      if (readyForGame(roomWithUpdatedUserStatus)) {
        handleStartGame(io, roomWithUpdatedUserStatus);
      }
    }
  });
};

const everyUserFinish = (users) => {
  return users.every((user) => user.progress === 100);
};

const handleSetProgress = (io, socket, { progress, symbolsLeft }) => {
  const id = socket.client.id;
  const rooms = Object.keys(socket.rooms);
  const currentRoomName = rooms.find((name) => storage.roomIsExist(name));
  if (!currentRoomName) return;

  const startGameTime =
    storage.getActiveRoom(currentRoomName).playingProcess.startGame;

  const playTime = Date.now() - startGameTime;

  if (playTime / 1000 > SECONDS_FOR_GAME) return;

  const currentStateRoom = storage.setUserProgress(
    currentRoomName,
    id,
    playTime,
    {
      progress,
      symbolsLeft,
    }
  );

  if (progress === 100) {
    commentatorBot.userFinished(io, id, currentRoomName);
  } else {
    commentatorBot.finishStraight(io, currentRoomName);
  }

  if (everyUserFinish(currentStateRoom.users)) {
    gameOver(io, currentStateRoom)();
    return;
  }

  io.to(currentRoomName).emit(
    "SET_PROGRESS",
    currentStateRoom.users.find((user) => user.id === id)
  );
};

const getRandomIndex = (mas) => {
  return Math.floor(Math.random() * mas.length);
};

const gameOver = (io, currentStateRoom) => {
  io.to(currentStateRoom.name).emit("GAME_OVER");
  commentatorBot.gameOver(io, currentStateRoom.name);

  const resetStateRoom = storage.resetActiveRoom(currentStateRoom.name);
  storage.deletePlayingRoom(currentStateRoom.name);

  io.to(currentStateRoom.name).emit("RESET_ROOM", resetStateRoom);

  if (
    resetStateRoom &&
    resetStateRoom.users.length !== MAXIMUM_USERS_FOR_ONE_ROOM
  ) {
    io.emit("ADD_ROOM", resetStateRoom);
  }

  return (interval) => {
    if (interval) {
      clearInterval(interval);
    }
  };
};

const handleStartGame = async (io, room) => {
  io.emit("REMOVE_ROOM", room.name);
  io.to(room.name).emit("PREPARE_GAME", getRandomIndex(texts));
  storage.addPlayingRoom(room.name);
  commentatorBot.start(io, room.name);
  await new Promise((resolve, reject) => {
    const startTime = Date.now();
    io.to(room.name).emit("START_COUNTDOWN", SECONDS_TIMER_BEFORE_START_GAME);
    const interval = setInterval(() => {
      if (!storage.roomIsExist(room.name)) {
        clearInterval(interval);
        resolve();
        return;
      }
      const currentTime = Date.now();
      const difference = Math.round((currentTime - startTime) / 1000);
      if (difference < SECONDS_TIMER_BEFORE_START_GAME) {
        const countdown = SECONDS_TIMER_BEFORE_START_GAME - difference;
        io.to(room.name).emit("START_COUNTDOWN", countdown);
      } else {
        io.to(room.name).emit("START_COUNTDOWN", 0);
        clearInterval(interval);
        resolve();
      }
    }, 1000);
  });
  if (!storage.roomIsExist(room.name)) return;
  io.to(room.name).emit("START_GAME", SECONDS_FOR_GAME);
  const startTime = storage.setActiveRoomStartGame(room.name);
  const interval = setInterval(() => {
    const currentStateRoom = storage.getActiveRoom(room.name);
    if (!currentStateRoom || !storage.roomIsPlaying(room.name)) {
      clearInterval(interval);
      return;
    }

    if (everyUserFinish(currentStateRoom.users)) {
      gameOver(io, currentStateRoom)(interval);
      return;
    }
    const currentTime = Date.now();
    const difference = Math.round((currentTime - startTime) / 1000);
    if (difference < SECONDS_FOR_GAME) {
      const countdown = SECONDS_FOR_GAME - difference;
      io.to(room.name).emit("GAME_COUNTDOWN", countdown);
      commentatorBot.currentState(io, room.name, currentTime);
      commentatorBot.joke(io, room.name, currentTime);
    } else {
      gameOver(io, currentStateRoom)(interval);
    }
  }, 1000);
};

export {
  handleToggleStatusReady,
  handleStartGame,
  handleSetProgress,
  readyForGame,
};
