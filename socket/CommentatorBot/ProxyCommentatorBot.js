import СommentatorBot from "./СommentatorBot";
import {
  SECONDS_BEFORE_UPDATE_STATE,
  FINISH_STRAIGHT_SYMBOLS_LEFT,
  SECONDS_BEFORE_UPDATE_JOKE
} from "./config";

//It is a Proxy that decides whether to transfer processing to a real bot
class ProxyCommentatorBot {
  constructor(storage) {
    this._storage = storage;
    this.realbot = new СommentatorBot(storage);
  }

  start(io, roomName) {
    this.realbot.start(io, roomName);
  }

  joke(io, roomName, currentTime){
    const room = this._storage.getActiveRoom(roomName);
    if (room.playingProcess.finishStraight) return;

    const difference = currentTime - room.playingProcess.lastTimeOfJoke;
    if (difference >= SECONDS_BEFORE_UPDATE_JOKE * 1000) {
      this.realbot.joke(io, roomName, currentTime);
    }
  }

  currentState(io, roomName, currentTime) {
    const room = this._storage.getActiveRoom(roomName);
    
    if (room.playingProcess.finishStraight) return;

    const difference = currentTime - room.playingProcess.lastTimeOfNoticeState;

    if (difference >= SECONDS_BEFORE_UPDATE_STATE * 1000) {
      this.realbot.currentState(io, roomName, currentTime);
    }
  }

  finishStraight(io, roomName) {
    const room = this._storage.getActiveRoom(roomName);
    if (room.playingProcess.finishStraight) return;

    const isFinistStraight = room.users.some(
      (user) => user.symbolsLeft && user.symbolsLeft <= FINISH_STRAIGHT_SYMBOLS_LEFT
    );

    if (isFinistStraight) {
      this.realbot.finishStraight(io, roomName);
    }
  }

  userFinished(io, id ,roomName) {
    this.realbot.userFinished(io, id ,roomName);
  }

  gameOver(io, roomName) {
    this.realbot.gameOver(io, roomName);
  }
}

export default ProxyCommentatorBot;
