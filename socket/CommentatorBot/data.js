const cars = [
  "Acura",
  "Audi",
  "Bentley",
  "Bugatti",
  "Buick",
  "Bentley",
  "Mazda",
];

const greeting = [
  "Hi, I am the presenter of the keyboard race. The game will start in a few seconds",
];

const jokes = [
  "Why are ghosts such bad liars? Because they are easy to see through.",
  "If we shouldn't eat at night, why do they put a light in the fridge?",
  "Where do fish sleep? In the riverbed.",
  "Some people think prison is one word…but to robbers it's the whole sentence.",
];

export { cars, greeting, jokes };
