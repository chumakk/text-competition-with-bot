import * as textPatterns from "./data";

class СommentatorBot {
  constructor(storage) {
    this._storage = storage;
  }

  start(io, roomName) {
    this._setCarsToUsers(roomName);
    io.to(roomName).emit(
      "SET_BOT_START_MESSAGE",
      ...this._createStartMessage(roomName)
    );
  }

  _setCarsToUsers(roomName) {
    const room = this._storage.getActiveRoom(roomName);
    const usersWithCars = room.users.map((user) => ({
      ...user,
      car: this._getRandomCar(),
    }));
    this._storage.setActiveRoom(roomName, {
      ...room,
      users: usersWithCars,
    });
  }

  _getRandomCar() {
    return textPatterns.cars[this._randomIndex(textPatterns.cars)];
  }

  _randomIndex(array) {
    return Math.floor(Math.random() * array.length);
  }

  _createStartMessage(roomName) {
    const room = this._storage.getActiveRoom(roomName);
    const greeting = this._getRandomCarGreeting();
    return [greeting, room.users];
  }

  _getRandomCarGreeting() {
    return textPatterns.greeting[this._randomIndex(textPatterns.greeting)];
  }

  joke(io, roomName, currentTime) {
    const room = this._storage.getActiveRoom(roomName);
    this._storage.setActiveRoom(roomName, {
      ...room,
      playingProcess: {
        ...room.playingProcess,
        lastTimeOfJoke: currentTime,
      },
    });
    const joke = this._getRandomJoke();
    io.to(roomName).emit("SET_BOT_JOKE_MESSAGE", joke);
  }

  _getRandomJoke() {
    return textPatterns.jokes[this._randomIndex(textPatterns.jokes)];
  }

  currentState(io, roomName, currentTime) {
    const room = this._storage.getActiveRoom(roomName);
    this._storage.setActiveRoom(roomName, {
      ...room,
      playingProcess: {
        ...room.playingProcess,
        lastTimeOfNoticeState: currentTime,
        lastTimeOfJoke: currentTime,
      },
    });
    io.to(roomName).emit("SET_BOT_CURRENT_STATE_MESSAGE", room.users);
  }

  finishStraight(io, roomName) {
    const room = this._storage.getActiveRoom(roomName);
    this._storage.setActiveRoom(roomName, {
      ...room,
      playingProcess: {
        ...room.playingProcess,
        finishStraight: true,
      },
    });
    io.to(roomName).emit("SET_BOT_FINISH_STRAIGHT_MESSAGE", room.users);
  }

  userFinished(io, id, roomName) {
    const room = this._storage.getActiveRoom(roomName);
    const user = room.users.find((user) => user.id === id);

    this._storage.setActiveRoom(roomName, {
      ...room,
      playingProcess: {
        ...room.playingProcess,
        lastTimeOfJoke: Date.now(),
      },
    });

    io.to(roomName).emit("SET_BOT_USER_FINISHED_MESSAGE", user);
  }

  gameOver(io, roomName) {
    const room = this._storage.getActiveRoom(roomName);
    io.to(roomName).emit("SET_BOT_GAME_OVER_MESSAGE", room.users);
  }
}

export default СommentatorBot;
