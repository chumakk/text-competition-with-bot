import storage from "../Logic";
import ProxyCommentatorBot from "./ProxyCommentatorBot";

const commentatorBot = new ProxyCommentatorBot(storage);

export default commentatorBot;