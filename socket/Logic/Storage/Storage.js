// It is a Facade that implements a user-friendly interface for 3 store
class Storage {
  constructor(activeUsersStore, activeRoomsStore, playingRoomsStore) {
    this._activeUsersStore = activeUsersStore;
    this._activeRoomsStore = activeRoomsStore;
    this._playingRoomsStore = playingRoomsStore;
  }

  deleteUserById(id) {
    const user = this.getUserById(id);
    this._activeUsersStore.deleteUser(user.name);
  }

  getUserById(id) {
    return this._activeUsersStore.getUserById(id);
  }

  userIsExist(name) {
    return this._activeUsersStore.userIsExist(name);
  }

  createUser(name, id) {
    this._activeUsersStore.setUser(name, id);
  }

  getActiveRooms() {
    const allAvailableRooms = this._activeRoomsStore.getActiveRooms();
    const rooms = allAvailableRooms.filter(
      (room) => !this.roomIsPlaying(room.name)
    );
    return rooms;
  }

  toggleUserPlayStatus(roomName, id) {
    const room = this.getActiveRoom(roomName);
    this.setActiveRoom(room.name, {
      ...room,
      users: room.users.map((user) =>
        user.id !== id ? user : { ...user, isReady: !user.isReady }
      ),
    });

    return this.getActiveRoom(room.name);
  }

  setUserProgress(roomName, id, playTime, { progress, symbolsLeft }) {
    const room = this.getActiveRoom(roomName);
    this.setActiveRoom(roomName, {
      ...room,
      users: room.users.map((user) => {
        if (user.id === id) {
          if (progress === 100) {
            user.finishTime = playTime;
          }
          user.progress = progress;
          user.symbolsLeft = symbolsLeft;
        }
        return user;
      }),
    });
    return this.getActiveRoom(room.name);
  }

  joinUserToActiveRoom(room, id) {
    this._activeRoomsStore.setRoom(room.name, {
      ...room,
      users: [...room.users, this._createUserForRoom(id)],
    });

    return this.getActiveRoom(room.name);
  }

  leaveFromActiveRoom(roomName, id) {
    const room = this.getActiveRoom(roomName);
    this.setActiveRoom(room.name, {
      ...room,
      users: room.users.filter((user) => user.id !== id),
    });

    return this.getActiveRoom(room.name);
  }

  roomIsExist(name) {
    return this._activeRoomsStore.roomIsExist(name);
  }

  getActiveRoom(name) {
    return this._activeRoomsStore.getRoom(name);
  }

  setActiveRoom(name, room) {
    this._activeRoomsStore.setRoom(name, room);
  }

  createRoom(name, id) {
    this.setActiveRoom(name, {
      name,
      users: [this._createUserForRoom(id)],
      playingProcess: {
        startGame: null,
        lastTimeOfJoke: null,
        lastTimeOfNoticeState: null,
        finishStraight: false,
      },
    });

    return this.getActiveRoom(name);
  }

  _createUserForRoom(id) {
    return {
      ...this._activeUsersStore.getUserById(id),
      isReady: false,
      progress: 0,
      finishTime: null,
      symbolsLeft: null,
      car: null,
    };
  }

  setActiveRoomStartGame(name) {
    const startGame = Date.now();
    const room = this.getActiveRoom(name);
    this.setActiveRoom(name, {
      ...room,
      playingProcess: {
        ...room.playingProcess,
        startGame,
        lastTimeOfNoticeState: startGame,
      },
    });
    return startGame;
  }

  resetActiveRoom(name) {
    const room = this.getActiveRoom(name);
    this.setActiveRoom(name, {
      ...room,
      users: room.users.map((user) => this._createUserForRoom(user.id)),
      playingProcess: {
        startGame: null,
        lastTimeOfJoke: null,
        lastTimeOfNoticeState: null,
        finishStraight: false,
      },
    });
    return this.getActiveRoom(name);
  }

  deleteActiveRoom(name) {
    this._activeRoomsStore.deteleRoom(name);
  }

  addPlayingRoom(name) {
    this._playingRoomsStore.addRoom(name);
  }

  roomIsPlaying(name) {
    return this._playingRoomsStore.roomIsPlaying(name);
  }

  deletePlayingRoom(name) {
    this._playingRoomsStore.deteleRoom(name);
  }
}

export default Storage;
