import StorageFactory from "./BasicClasses";
import Storage from "./Storage/Storage";

const activeUsers = new Map();
const activeRooms = new Map();
const playingRooms = new Set();

const storageFactory = new StorageFactory();

const activeUsersStore = storageFactory.createStorage(
  "activeUsers",
  activeUsers
);

const activeRoomsStore = storageFactory.createStorage(
  "activeRooms",
  activeRooms
);

const playingRoomsStore = storageFactory.createStorage(
  "playingRooms",
  playingRooms
);

const storage = new Storage(
  activeUsersStore,
  activeRoomsStore,
  playingRoomsStore
);

export default storage;
