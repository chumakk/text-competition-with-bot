import ActiveUsers from "./ActiveUsersStorage";
import ActiveRooms from "./ActiveRoomsStorage";
import PlayingRooms from "./PlayingRoomsStorage";

// This is a Factory of stores, which creates a store depending on the passed parameter
class StorageFactory {
  createStorage(name, store) {
    switch (name) {
      case "activeUsers":
        return new ActiveUsers(store);

      case "activeRooms":
        return new ActiveRooms(store);

      case "playingRooms":
        return new PlayingRooms(store);

      default:
        break;
    }
  }
}

export default StorageFactory;
