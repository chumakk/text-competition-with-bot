import { MAXIMUM_USERS_FOR_ONE_ROOM } from "../../config";

class ActiveRooms {
  constructor(store, activeUsersStore) {
    this._store = store;
    this._activeUsersStore = activeUsersStore;
  }

  getRoom(name) {
    return this._store.get(name);
  }

  setRoom(name, room) {
    this._store.set(name, room);
  }

  roomIsExist(name) {
    return this._store.has(name);
  }

  getActiveRooms() {
    const rooms = [];
    this._store.forEach((room) => {
      if (room.users.length !== MAXIMUM_USERS_FOR_ONE_ROOM) {
        rooms.push(room);
      }
    });
    return rooms;
  }

  deteleRoom(name){
    this._store.delete(name)
  }
}

export default ActiveRooms;
