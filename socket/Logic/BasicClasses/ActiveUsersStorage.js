class ActiveUsers {
  constructor(store) {
    this._store = store;
  }

  setUser(name, id) {
    this._store.set(name, id);
  }

  userIsExist(name) {
    return this._store.has(name);
  }

  getUserById(id) {
    for (const user of this._store) {
      if (user[1] === id) {
        return { name: user[0], id: user[1] };
      }
    }
  }

  deleteUser(name) {
    this._store.delete(name);
  }
}

export default ActiveUsers;
