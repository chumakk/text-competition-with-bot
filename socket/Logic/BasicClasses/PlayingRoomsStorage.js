class PlayingRooms {
  constructor(store) {
    this._store = store;
  }

  addRoom(name) {
    this._store.add(name);
  }

  roomIsPlaying(name) {
    return this._store.has(name);
  }

  deteleRoom(name) {
    this._store.delete(name);
  }
}

export default PlayingRooms;
