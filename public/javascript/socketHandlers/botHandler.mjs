import {
  createGreeting,
  createListOfUsers,
  currentStateUser,
  gameOverUser,
  botMessage,
} from "../views/bot.mjs";
import { botMessageBox } from "../global.mjs";

const sortUserBy = (name) => (user, nextUser) => {
  if (!user[name] || !nextUser[name]) {
    if (!user[name] && !nextUser[name]) return 0;
    if (!user[name]) return 1;
    if (!nextUser[name]) return -1;
  }
  return user[name] - nextUser[name];
};

const handleStartBotMessage = (greeting, users) => {
  const greetingEl = createGreeting(greeting);
  const usersInfoEl = createListOfUsers(users);
  botMessageBox.append(greetingEl, usersInfoEl);
};

const handleFinishStraightBotMessage = (users) => {
  botMessageBox.innerHTML = "";
  const sorted = [...users].sort(sortUserBy("symbolsLeft"));
  const message = botMessage(
    `Not much left until the end of the race, the <span class="message-marker">${sorted[0].name} on ${sorted[0].car}</span> can finish first, but let's wait for the finish)`
  );

  botMessageBox.append(message);
};

const handleGameOverBotMessage = (users) => {
  botMessageBox.innerHTML = "";
  const usersInfo = users
    .sort(sortUserBy("finishTime"))
    .map((user, index) => gameOverUser(user, index));
  const message = botMessage("This is the end friends, here are the results:");
  botMessageBox.append(message, ...usersInfo);
};

const handleUserFinishedBotMessage = (user) => {
  botMessageBox.innerHTML = "";
  const message = botMessage(`The user <span class="message-marker">${user.name} on ${user.car}</span> finished`);
  botMessageBox.append(message);
};

const handleJokeBotMessage = (msg) => {
  botMessageBox.innerHTML = "";
  const message = botMessage(msg);
  botMessageBox.append(message);
};

const handleСurrentStateBotMessage = (users) => {
  botMessageBox.innerHTML = "";
  const usersInfo = users
    .sort(sortUserBy("symbolsLeft"))
    .map((user, index) => currentStateUser(user, index));
  const message = botMessage("Current state of race:");
  botMessageBox.append(message, ...usersInfo);
};

export {
  handleStartBotMessage,
  handleСurrentStateBotMessage,
  handleFinishStraightBotMessage,
  handleUserFinishedBotMessage,
  handleGameOverBotMessage,
  handleJokeBotMessage,
};
