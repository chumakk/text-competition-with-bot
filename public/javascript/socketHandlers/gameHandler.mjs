import { addClass, removeClass } from "../helpers/helper.mjs";
import {
  readyUserBtn,
  leaveRoomBtn,
  timer,
  gameText,
  gameTimer,
  botMessageBox,
  botMessageBoxWrapper,
} from "../global.mjs";
import { updateUserStatus } from "../views/user.mjs";
import { setTextGame, onInputTextWrapper } from "../listeners/game.mjs";

let inputHandler;

const handleToggleStatusReady = (socket, user) => {
  updateUserStatus(user);
  if (socket.id === user.id) {
    readyUserBtn.innerHTML = user.isReady ? "NOT READY" : "READY";
  }
};

const handlePrepareGame = (index) => {
  addClass(readyUserBtn, "display-none");
  addClass(leaveRoomBtn, "not-visible");
  removeClass(timer, "display-none");
  fetch(`/game/texts/${index}`, {
    method: "GET",
  })
    .then((res) => res.text())
    .then((text) => setTextGame(text));
  removeClass(botMessageBoxWrapper, "display-none");
  botMessageBox.innerHTML = "";
};

const handleStartCountdown = (time) => {
  timer.innerHTML = time;
};

const handleStartGame = (socket, seconds) => {
  addClass(timer, "display-none");
  timer.innerHTML = "";
  removeClass(gameText, "display-none");
  removeClass(gameTimer, "display-none");
  gameTimer.innerHTML = `${seconds} seconds left`;
  inputHandler = onInputTextWrapper(socket);
  document.addEventListener("keydown", inputHandler);
};

const handleGameOver = () => {
  document.removeEventListener("keydown", inputHandler);
  addClass(gameText, "display-none");
  addClass(gameTimer, "display-none");
  gameText.innerHTML = "";
  gameTimer.innerHTML = "";
};

const handleGameCountDown = (seconds) => {
  gameTimer.innerHTML = `${seconds} seconds left`;
};

const handleSetProgress = (user) => {
  const progressBar = document.querySelector(`.user-progress.${user.name}`);
  if (user.progress === 100) addClass(progressBar, "finished");
  progressBar.style.width = `${user.progress}%`;
};

export {
  handleToggleStatusReady,
  handlePrepareGame,
  handleStartCountdown,
  handleStartGame,
  handleGameOver,
  handleGameCountDown,
  handleSetProgress,
};
