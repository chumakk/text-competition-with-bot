import { createElement, addClass } from "../helpers/helper.mjs";

const createGreeting = (message) => {
  const greeting = createElement({ tagName: "div", className: "bot-greeting" });
  greeting.append(message);
  return greeting;
};

const createListUser = (user, index) => {
  const userEl = createElement({
    tagName: "div",
    className: "going-user",
  });
  userEl.append(`${user.name} on ${user.car} by number - ${index + 1}`);
  return userEl;
};

const createListOfUsers = (users) => {
  const listOfUsers = createElement({
    tagName: "div",
    className: "bot-list-users",
  });
  const usersEl = users.map((user, index) => createListUser(user, index));
  const title = createElement({ tagName: "div", className: "bot-title" });
  title.append("List of riders:");
  listOfUsers.append(title, ...usersEl);
  return listOfUsers;
};

const currentStateUser = (user, index) => {
  const userEl = createElement({
    tagName: "div",
    className: "going-user",
  });

  if (user.symbolsLeft) {
    userEl.append(
      `${index + 1}. ${user.name} - ${user.symbolsLeft} symbols left`
    );
  } else {
    userEl.append(`${index + 1}. ${user.name}`);
  }
  return userEl;
};

const gameOverUser = (user, index) => {
  const userEl = createElement({
    tagName: "div",
    className: `going-user`,
  });
  if (user.finishTime) {
    addClass(userEl, `place-${index + 1}`);
    if (index < 3) {
      userEl.append(
        `${index + 1}. ${user.name} on ${user.car} with time - ${(
          user.finishTime / 1000
        ).toFixed(2)}s`
      );
    } else {
      userEl.append(`${index + 1}. ${user.name} on ${user.car}`);
    }
  } else {
    userEl.append(`${user.name} didn't finish`);
  }

  return userEl;
};

const botMessage = (msg) => {
  const message = createElement({ tagName: "div", className: "bot-message" });
  message.innerHTML = msg;
  return message;
};

export {
  createGreeting,
  createListOfUsers,
  currentStateUser,
  gameOverUser,
  botMessage,
};
