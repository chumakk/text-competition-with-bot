const roomContainer = document.querySelector(".room-container");
const roomPage = document.querySelector("#rooms-page");
const gamePage = document.querySelector("#game-page");
const gameUsersContainer = document.querySelector(".room-users");
const readyUserBtn = document.querySelector("#ready-btn");
const leaveRoomBtn = document.querySelector("#quit-room-btn");
const timer = document.querySelector("#timer");
const gameTimer = document.querySelector("#gameTimer");
const gameText = document.querySelector("#text-container");
const roomNameContainer = document.querySelector(".room-name");
const botMessageBox = document.querySelector("#bot-message-box");
const botMessageBoxWrapper = document.querySelector(".bot-message-wrapper");

export {
  roomContainer,
  roomPage,
  gamePage,
  gameUsersContainer,
  readyUserBtn,
  leaveRoomBtn,
  timer,
  gameTimer,
  gameText,
  roomNameContainer,
  botMessageBox,
  botMessageBoxWrapper
};
